package com.wealert.flutter_applovin

import android.content.Context
import android.util.Log
import com.applovin.mediation.MaxAd
import com.applovin.mediation.MaxError
import com.applovin.mediation.nativeAds.MaxNativeAdListener
import com.applovin.mediation.nativeAds.MaxNativeAdLoader
import com.applovin.mediation.nativeAds.MaxNativeAdView
import com.applovin.mediation.nativeAds.MaxNativeAdViewBinder
import com.applovin.sdk.AppLovinSdk
import io.flutter.plugin.common.BinaryMessenger
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel


class NativeApplovinController(
    val id: String,
    private val channel: MethodChannel,
    private val context: Context
) : MethodChannel.MethodCallHandler {

    enum class CallMethod {
        setAdUnitID, reloadAd
    }

    enum class LoadState {
        loading, loadError, loadCompleted
    }

    var nativeAdChanged: ((MaxNativeAdView?) -> Unit)? = null
    var loadedNativeAd: MaxAd? = null
        set(value) {
            field = value
            invokeLoadCompleted()
        }
    var loadedAdView: MaxNativeAdView? = null
    var adLoader: MaxNativeAdLoader? = null

    private var adUnitID: String? = null

    init {
        channel.setMethodCallHandler(this)

    }

    override fun onMethodCall(call: MethodCall, result: MethodChannel.Result) {
        when (CallMethod.valueOf(call.method)) {
            CallMethod.setAdUnitID -> {
                call.argument<String>("adUnitID")?.let {
                    val isChanged = adUnitID != it
                    adUnitID = it

                    if (adLoader == null || isChanged) {
                        adLoader = MaxNativeAdLoader(adUnitID, this.context)
                        adLoader?.setNativeAdListener(NativeAdListener())
                    }

                    val postCode: Number? = call.argument<Number>("postCode")
                    val postCity: Int? = call.argument<Int>("postCity")

                    if (loadedNativeAd == null || isChanged) loadAd(postCode, postCity) else invokeLoadCompleted()
                } ?: result.success(null)
            }

            CallMethod.reloadAd -> {
                call.argument<Boolean>("forceRefresh")?.let {
                    val postCode: Number? = call.argument<Number>("postCode")
                    val postCity: Int? = call.argument<Int>("postCity")
                    if (it || adLoader == null) loadAd(postCode, postCity) else invokeLoadCompleted()
                }
            }
        }
    }

    private fun createNativeAdView(): MaxNativeAdView {
        val binder: MaxNativeAdViewBinder =
            MaxNativeAdViewBinder.Builder(R.layout.native_ad_layout)
                .setTitleTextViewId(R.id.ad_title)
                .setMediaContentViewGroupId(R.id.media_view_container)
                .setOptionsContentViewGroupId(R.id.ad_options_view)
                .setCallToActionButtonId(R.id.call_to_action)
                .build()
        return MaxNativeAdView(binder, this.context)
    }

    private fun loadAd(postCode: Number?, postCity: Int?) {
        channel.invokeMethod(LoadState.loading.toString(), null)
        val keyword : MutableList<String> = mutableListOf()
        postCode?.let {
            keyword.add("postCode:${it.toString().replace(".0", "")}")
        }
        postCity?.let {
            keyword.add("postCity:$it")
        }
        if (keyword.size > 0) {
            AppLovinSdk.getInstance( context ).targetingData.keywords = keyword
        }
        print("ApplovinTargeting: ${AppLovinSdk.getInstance( context ).targetingData.keywords}")
        adLoader?.loadAd(createNativeAdView())
    }

    private fun invokeLoadCompleted() {
        nativeAdChanged?.let { it(loadedAdView) }
        channel.invokeMethod(LoadState.loadCompleted.toString(), null)
    }

    private inner class NativeAdListener : MaxNativeAdListener()
    {
        override fun onNativeAdLoaded(nativeAdView: MaxNativeAdView?, nativeAd: MaxAd) {
            Log.d("Applovin", "Native ad ${nativeAd.adUnitId} has loaded.")
            // Clean up any pre-existing native ad to prevent memory leaks.
            if (loadedNativeAd != null) {
                val tmp = loadedNativeAd
                adLoader?.destroy(tmp)
            }

            // Save ad for cleanup.
            loadedNativeAd = nativeAd
            loadedAdView = nativeAdView
        }

        override fun onNativeAdLoadFailed(adUnitId: String, error: MaxError) {
            Log.d("Applovin", "Native ad $adUnitId failed to load with error: $error")
            channel.invokeMethod(LoadState.loadError.toString(), null)
        }

        override fun onNativeAdClicked(nativeAd: MaxAd) {}
    }
}

object NativeApplovinControllerManager {
    private val CONTROLLERS: ArrayList<NativeApplovinController> = arrayListOf()

    fun createController(id: String, binaryMessenger: BinaryMessenger, context: Context) {
        if (getController(id) == null) {
            val methodChannel = MethodChannel(binaryMessenger, id)
            val controller = NativeApplovinController(id, methodChannel, context)
            CONTROLLERS.add(controller)
        }
    }

    fun getController(id: String): NativeApplovinController? {
        return CONTROLLERS.firstOrNull { it.id == id }
    }

    fun removeController(id: String) {
        val index = CONTROLLERS.indexOfFirst { it.id == id }
        if (index >= 0) CONTROLLERS.removeAt(index)
    }
}