package com.wealert.flutter_applovin

import android.content.Context
import android.content.res.Resources
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.NonNull
import com.applovin.sdk.AppLovinSdk
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.RequestConfiguration
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.BinaryMessenger
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.StandardMessageCodec
import io.flutter.plugin.platform.PlatformView
import io.flutter.plugin.platform.PlatformViewFactory

/** FlutterApplovinPlugin */
class FlutterApplovinPlugin: FlutterPlugin, MethodCallHandler {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private lateinit var channel : MethodChannel
  private lateinit var messenger: BinaryMessenger
  private lateinit var context: Context
  private val viewType = "native_admob"

  enum class CallMethod {
    initController, disposeController, setTestDeviceIds, openMediationDebugger
  }

  override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
    channel = MethodChannel(flutterPluginBinding.binaryMessenger, "flutter_applovin")
    messenger = flutterPluginBinding.binaryMessenger
    context = flutterPluginBinding.applicationContext
    channel.setMethodCallHandler(this)

    flutterPluginBinding.platformViewRegistry
      .registerViewFactory(viewType, CustomViewFactory())
    init()
  }

  fun init() {
    // Make sure to set the mediation provider value to "max" to ensure proper functionality
    AppLovinSdk.getInstance( context ).mediationProvider = "max"
    AppLovinSdk.getInstance( context ).initializeSdk()
    AppLovinSdk.getInstance( context ).settings.setVerboseLogging( false )
  }

  override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
    when (CallMethod.valueOf(call.method)) {
      CallMethod.initController -> {
        (call.argument<String>("controllerID"))?.let {
          NativeApplovinControllerManager.createController(it, messenger, context)
        }
      }

      CallMethod.disposeController -> {
        (call.argument<String>("controllerID"))?.let {
          NativeApplovinControllerManager.removeController(it)
        }
      }

      CallMethod.setTestDeviceIds -> {
        (call.argument<List<String>>("testDeviceIds"))?.let {
          val configuration = RequestConfiguration.Builder().setTestDeviceIds(it).build()
          MobileAds.setRequestConfiguration(configuration)
        }
      }
      CallMethod.openMediationDebugger -> {
        AppLovinSdk.getInstance( context ).showMediationDebugger()
      }
    }
  }

  override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
    channel.setMethodCallHandler(null)
  }
}

class CustomViewFactory : PlatformViewFactory(StandardMessageCodec.INSTANCE) {

  override fun create(context: Context, id: Int, params: Any?): PlatformView {
    return NativePlatformView(context, id, params)
  }
}

class NativePlatformView(
  context: Context,
  id: Int,
  params: Any?
) : PlatformView {

  private var controller: NativeApplovinController? = null
  private var view = View(context)
  private var options = NativeAdOptions()

  init {
    val map = params as HashMap<*, *>

    (map["options"] as? HashMap<*, *>)?.let { optionMap ->
      options = NativeAdOptions.parse(optionMap)
    }
    Log.d("Applovin", "update options")

    (map["controllerID"] as? String)?.let { it ->
      val controller = NativeApplovinControllerManager.getController(it)
      this.controller = controller
    }

    controller?.loadedAdView?.let {
      val vg = it as ViewGroup
      for (i in 0 until vg.childCount) {
        val child = vg.getChildAt(i)
        if((child is TextView)){
          if(child.id == R.id.ad_title){
            child.setTextColor(options.headlineTextStyle.color)
          }
        }
      }
      // Add the ad view to our view hierarchy
      view = it
    }
  }

  override fun getView(): View = view

  override fun dispose() {}
}

fun Int.toRoundedColor(radius: Float): Drawable {
  val drawable = GradientDrawable()
  drawable.shape = GradientDrawable.RECTANGLE
  drawable.cornerRadius = radius * Resources.getSystem().displayMetrics.density
  drawable.setColor(this)
  return drawable
}

fun Int.dp(): Int {
  val density = Resources.getSystem().displayMetrics.density
  return (this * density).toInt()
}
