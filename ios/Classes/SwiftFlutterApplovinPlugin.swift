import Flutter
import UIKit
import AppLovinSDK


let viewType = "native_admob"
let maxControllerManager = NativeMaxControllerManager.shared

public class SwiftFlutterApplovinPlugin: NSObject, FlutterPlugin {
    
  enum CallMethod: String {
      case initController
      case disposeController
  }
  let messenger: FlutterBinaryMessenger
    
  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "flutter_applovin", binaryMessenger: registrar.messenger())
    let instance = SwiftFlutterApplovinPlugin(binaryMessenger: registrar.messenger())
    registrar.addMethodCallDelegate(instance, channel: channel)
    let viewFactory = MaxPlatformViewFactory()
    registrar.register(viewFactory, withId: viewType)
  }
    
    init(binaryMessenger: FlutterBinaryMessenger) {
        self.messenger = binaryMessenger
    }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
      guard let callMethod = CallMethod(rawValue: call.method) else { return result(FlutterMethodNotImplemented) }
      let params = call.arguments as? [String: Any]
      
      switch callMethod {
      case .initController:
          if let controllerID = params?["controllerID"] as? String {
              maxControllerManager.createController(forID: controllerID, binaryMessenger: messenger)
          }
          
      case .disposeController:
          if let controllerID = params?["controllerID"] as? String {
              maxControllerManager.removeController(forID: controllerID)
          }
      }
      
      result(nil)
  }
}

class MaxPlatformViewFactory: NSObject, FlutterPlatformViewFactory {
    
    func create(withFrame frame: CGRect, viewIdentifier viewId: Int64, arguments args: Any?) -> FlutterPlatformView {
        return MaxPlatformView(frame, viewId: viewId, args: args)
    }
    
    func createArgsCodec() -> FlutterMessageCodec & NSObjectProtocol {
        return FlutterStandardMessageCodec.sharedInstance()
    }
}

class MaxPlatformView: NSObject, FlutterPlatformView {
    
    private var controller: NativeMaxController?

    private let adContainerView = UIView()
    private let params: [String: Any]
    private var options = NativeAdOptions()
    
    init(_ frame: CGRect, viewId: Int64, args: Any?) {
        params = args as? [String: Any] ?? [:]
        
        if let controllerID = params["controllerID"] as? String,
            let controller = maxControllerManager.getController(forID: controllerID) {
            self.controller = controller
        }
        
        if let data = params["options"] as? [String: Any] {
            options = NativeAdOptions(data)
        }
        
        // Set native ad
        if let loadedView = controller?.loadedAdView {
            do {
                if let v = loadedView.viewWithTag(1), let vLabel = v as? UILabel{
                    vLabel.textColor = options.headlineTextStyle.color
                }
                for v in adContainerView.subviews {
                    v.removeFromSuperview()
                }
                self.adContainerView.addSubview(loadedView)
                
                loadedView.translatesAutoresizingMaskIntoConstraints = false
                let constraints: [NSLayoutConstraint] = [
                    loadedView.topAnchor.constraint(equalTo: adContainerView.topAnchor),
                    loadedView.bottomAnchor.constraint(equalTo: adContainerView.bottomAnchor),
                    loadedView.leadingAnchor.constraint(equalTo: adContainerView.leadingAnchor),
                    loadedView.trailingAnchor.constraint(equalTo: adContainerView.trailingAnchor),
                ]
                NSLayoutConstraint.activate(constraints)
            } catch {
            }
        }
        
        super.init()
    }
    
    func view() -> UIView {
        return adContainerView
    }
}
