//
//  NativeAdmobController.swift
//  flutter_native_admob
//
//  Created by Dao Duy Duong on 3/14/20.
//

import UIKit
import AppLovinSDK

class NativeMaxController: NSObject, MANativeAdDelegate, MAAdRevenueDelegate {
    enum CallMethod: String {
        case setAdUnitID
        case reloadAd
    }
    
    enum LoadState: String {
        case loading, loadError, loadCompleted
    }
    
    let id: String
    let channel: FlutterMethodChannel
    
    var nativeAdChanged: ((MANativeAdView?) -> Void)?
    var loadedAdView: MANativeAdView?
    var nativeAd: MAAd?
    
    private var nativeAdLoader: MANativeAdLoader?
    private var adUnitID: String?
    
    private var isLoading = false
    
    init(id: String, channel: FlutterMethodChannel) {
        self.id = id
        self.channel = channel
        super.init()
        
        channel.setMethodCallHandler(handle)
    }
    
    private func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        guard let callMethod = CallMethod(rawValue: call.method) else { return result(FlutterMethodNotImplemented) }
        let params = call.arguments as? [String: Any]
        
        switch callMethod {
        case .setAdUnitID:
//            print("[AppLovin] ========= set ad and load ad =========")
            
            guard let adUnitID = params?["adUnitID"] as? String else {
                return result(nil)
            }
            
            var postCode: Float?
            var postCity: Int?
            if let postcode = params?["postCode"] as? Float {
                postCode = postcode
            }
            
            if let postcity = params?["postCity"] as? Int {
                postCity = postcity
            }
            
            let isChanged = adUnitID != self.adUnitID
            self.adUnitID = adUnitID
            
            if loadedAdView == nil || isChanged {
                guard !isLoading else {
//                    print("[AppLovin] isLoading \(self.adUnitID)")
                    channel.invokeMethod(LoadState.loading.rawValue, arguments: nil)
                    return
                }
                isLoading = true
                nativeAdLoader = MANativeAdLoader(adUnitIdentifier: adUnitID)
                nativeAdLoader?.nativeAdDelegate = self;
                nativeAdLoader?.revenueDelegate = self;
                loadAd(postCode, postCity)
            } else {
//                print("[AppLovin] has ad \(self.adUnitID)")
                invokeLoadCompleted()
            }
            
        case .reloadAd:
            var postCode: Float?
            var postCity: Int?
            if let postcode = params?["postCode"] as? Float {
                postCode = postcode
            }
            if let postcity = params?["postCity"] as? Int {
                postCity = postcity
            }
            
//            print("[AppLovin] ========= reload ad =========")
            
            guard !isLoading else {
//                print("[AppLovin] isLoading \(self.adUnitID)")
                channel.invokeMethod(LoadState.loading.rawValue, arguments: nil)
                return
            }
            
            loadedAdView = nil
            nativeAd = nil
            isLoading = true
            loadAd(postCode, postCity)
        }
        
        result(nil)
    }
    
    private func loadAd(_ postCode: Float?, _ postCity: Int?) {
        channel.invokeMethod(LoadState.loading.rawValue, arguments: nil)
        
        var keyword: [String] = []
        if let code = postCode {
            keyword.append("postCode:\(String(describing: code).replacingOccurrences(of: ".0", with: ""))")
        }
        if let city = postCity {
            keyword.append("postCity:\(city)")
        }
        if keyword.count > 0 {
            ALSdk.shared()!.targetingData.keywords = keyword
        }
        print("ApplovinTargeting: \(ALSdk.shared()!.targetingData.keywords)")
        nativeAdLoader?.setLocalExtraParameterForKey("google_native_ad_view_tag", value: 5)
        nativeAdLoader?.loadAd(into: createNativeAdView())
    }
    
    func createNativeAdView() -> MANativeAdView {
        guard let nibObjects = Bundle.main.loadNibNamed("MaxNativeView", owner: nil, options: nil),
            let nativeAdView = nibObjects.first as? MANativeAdView else {
                fatalError("Could not load nib file for adView")
        }
        
        let adViewBinder = MANativeAdViewBinder.init(builderBlock: { (builder) in
            builder.titleLabelTag = 1
            builder.mediaContentViewTag = 2
            builder.callToActionButtonTag = 3
            builder.optionsContentViewTag = 4
        })
        nativeAdView.bindViews(with: adViewBinder)
        
        return nativeAdView
    }
    
    func didLoadNativeAd(_ nativeAdView: MANativeAdView?, for ad: MAAd) {
        if let n = nativeAd {
            nativeAdLoader?.destroy(n)
        }
        
        nativeAd = ad
        loadedAdView = nativeAdView
        self.isLoading = false
        self.invokeLoadCompleted()
        print("[AppLovin] load ad complete \(self.adUnitID)")
    }
    
    func didFailToLoadNativeAd(forAdUnitIdentifier adUnitIdentifier: String, withError error: MAError) {
        // Native ad load failed
        // We recommend retrying with exponentially higher delays up to a maximum delay
        self.isLoading = false
        self.channel.invokeMethod(LoadState.loadError.rawValue, arguments: nil)
        print("[AppLovin] load ad failed \(self.adUnitID)")
    }
    
    func didClickNativeAd(_ ad: MAAd) { }
    
    private func invokeLoadCompleted() {
        nativeAdChanged?(loadedAdView)
        channel.invokeMethod(LoadState.loadCompleted.rawValue, arguments: nil)
    }
    
    func viewControllerForPresentingModalView() -> UIViewController! {
        let app = UIApplication.shared.delegate as! FlutterAppDelegate
        if let rootViewController = app.window.rootViewController{
            return rootViewController
        }else {
            return UIViewController()
        }
    }
    
    func didPayRevenue(for ad: MAAd) { }
    
}

class NativeMaxControllerManager {
    
    static let shared = NativeMaxControllerManager()
    
    private var controllers: [NativeMaxController] = []
    
    private init() {}
    
    func createController(forID id: String, binaryMessenger: FlutterBinaryMessenger) {
        if getController(forID: id) == nil {
            let methodChannel = FlutterMethodChannel(name: id, binaryMessenger: binaryMessenger)
            let controller = NativeMaxController(id: id, channel: methodChannel)
            controllers.append(controller)
        }
    }
    
    func getController(forID id: String) -> NativeMaxController? {
        return controllers.first(where: { $0.id == id })
    }
    
    func removeController(forID id: String) {
        if let index = controllers.firstIndex(where: { $0.id == id }) {
            controllers.remove(at: index)
        }
    }
}
