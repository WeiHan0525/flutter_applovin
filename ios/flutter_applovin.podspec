#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html.
# Run `pod lib lint flutter_applovin.podspec` to validate before publishing.
#
Pod::Spec.new do |s|
  s.name             = 'flutter_applovin'
  s.version          = '0.0.1'
  s.summary          = 'A new Flutter project.'
  s.description      = <<-DESC
A new Flutter project.
                       DESC
  s.homepage         = 'http://example.com'
  s.license          = { :file => '../LICENSE' }
  s.author           = { 'Your Company' => 'email@example.com' }
  s.source           = { :path => '.' }
  s.source_files = 'Classes/**/*'
  s.public_header_files = 'Classes/**/*.h'
  s.ios.deployment_target = '11.0'
  s.dependency 'Flutter'
  s.dependency 'AppLovinSDK', '11.11.3'
  s.dependency 'AppLovinMediationFacebookAdapter', '6.14.0.0'
  s.dependency 'AppLovinMediationGoogleAdapter', '10.11.0.0'

  s.static_framework = true
end
