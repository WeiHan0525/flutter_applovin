
import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_applovin/native_max_controller.dart';
import 'package:flutter_applovin/native_max_options.dart';


const _viewType = "native_admob";

enum NativeMaxType { banner, full }

class NativeMax extends StatefulWidget {
  final String adUnitID;
  final NativeMaxOptions? options;
  final NativeMaxType? type;
  final int numberAds;
  final Widget? loading;
  final Widget? error;

  final num? postCode;
  final num? postCity;

  final NativeMaxController? controller;

  NativeMax({
    Key? key,
    required this.adUnitID,
    this.postCode,
    this.postCity,
    this.options,
    this.loading,
    this.error,
    this.controller,
    this.numberAds = 1,
    this.type = NativeMaxType.full,
  })  : assert(adUnitID.isNotEmpty),
        super(key: key);

  @override
  _NativeMaxState createState() => _NativeMaxState();
}
class _NativeMaxState extends State<NativeMax> with AutomaticKeepAliveClientMixin {
  static final isAndroid = defaultTargetPlatform == TargetPlatform.android;
  static final isiOS = defaultTargetPlatform == TargetPlatform.iOS;

  late NativeMaxController _nativeAdController;

  NativeMaxOptions get _options => widget.options ?? NativeMaxOptions();
  NativeMaxType get _type => widget.type ?? NativeMaxType.full;

  Widget get _loading =>
      widget.loading ?? Center(child: CircularProgressIndicator());

  Widget get _error => widget.error ?? Container();

  var _loadState = AdLoadState.loading;
  late StreamSubscription _subscription;

  @override
  void initState() {
    _nativeAdController = widget.controller ?? NativeMaxController();
    _nativeAdController.setAd(widget.adUnitID, widget.postCode, widget.postCity, numberAds: widget.numberAds);

    _subscription = _nativeAdController.stateChanged.listen((state) {
      setState(() {
        _loadState = state;
      });
    });

    print("${widget.adUnitID} init");
    super.initState();
  }

  @override
  void dispose() {
    _subscription.cancel();

    // We only dispose internal controller, external controller will be kept
    if (widget.controller == null) _nativeAdController.dispose();
    super.dispose();
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    if (isAndroid || isiOS) {
      return Container(
        child: _loadState == AdLoadState.loading
            ? _loading
            : _loadState == AdLoadState.loadError
            ? _error
            : _createPlatformView(),
      );
    }

    return Text('$defaultTargetPlatform is not supported PlatformView yet.');
  }

  Widget _createPlatformView() {
    final creationParams = {
      "options": _options.toJson(),
      "controllerID": _nativeAdController.id,
      "type": _type.toString().replaceAll("NativeAdmobType.", ""),
    };

    return isAndroid
        ? AndroidView(
      viewType: _viewType,
      creationParamsCodec: StandardMessageCodec(),
      creationParams: creationParams,
    )
        : UiKitView(
      viewType: _viewType,
      creationParamsCodec: StandardMessageCodec(),
      creationParams: creationParams,
    );
  }
}